</main>

<footer>
    <?php
    wp_footer();
    ?>
    <?php if (is_page('home')) : ?>
        <div class="footer-title">
            VISITE NOSSA LOJA FISÍCA
        </div>

        <div class="visit-store">
            <div class="footer-left-goup">
                <?php
                if (is_active_sidebar('map-text')) {
                    dynamic_sidebar('map-text');
                }
                ?>
            </div>
            <div class="footer-right-goup">
                <?php
                if (is_active_sidebar('visit-image')) {
                    dynamic_sidebar('visit-image');
                }
                ?>
            </div>
        </div>
    <? endif; ?>
    <div class="copyright">
        © Copyright 2021 - Desenvolvido por Time 7
    </div>

</footer>
</body>


</html>