<form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url(home_url('/')) ?>">
    <div>
        <label class="screen-reader-text" for="s">Faça seu pedido:</label>
        <input class="header-search-bar" type="text" value="<?php get_search_query(); ?>" name="s" id="s">
        <input class='btn-order' type="submit" id="searchsubmit" value="Faça seu pedido">

    </div>
    <div></div>
</form>