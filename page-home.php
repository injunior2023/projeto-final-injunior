<?php
get_header();
?>
<section class="header-section">
    <h1>Comes&Bebes</h1>
    <p>O restaurante para todas as fomes</p>
</section>

<section class="principal">
    <div class="p-title">
        <h2>
            CONHEÇA NOSSA LOJA
        </h2>
    </div>

    <div class="p-lists">
        <div class="list-category">

        </div>
        <div class="list-product">

            <?php
            $args = array(
                'post_type' => 'product',
                'posts_per_page' => 4
            );
            $loop = new WP_Query($args);


            if ($loop->have_posts()) {
                while ($loop->have_posts()) : $loop->the_post();


                //wc_get_template_part('content', 'single-product');
                //wc_get_template_part('content', 'product');
                endwhile;
            } else {
                echo __('No products found');
            }
            wp_reset_postdata();
            ?>
        </div>
    </div>
    <div class="p-btn">
        <button class="btn-options">
            Veja outras opções
        </button>
    </div>
</section>

<?php
get_footer();
?>