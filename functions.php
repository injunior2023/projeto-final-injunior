<?php

//Função para css/js
function comes_bebes_load_scripts()
{
    //Enfileirando normalize.css
    wp_enqueue_style('comes-bebes-normalize', get_template_directory_uri() . '/assets/css/normalize.css', [], '1.1', 'all');

    //Enfileirando Style.css
    wp_enqueue_style('comes-bebes-style', get_stylesheet_uri(), [], '1.1', false);

    //Enfileirando fonts do google
    wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css2?family=Bellota+Text:wght@300;400&family=Roboto:ital,wght@0,400;0,500;0,900;1,300&display=swap', [], null);
}
add_action('wp_enqueue_scripts', 'comes_bebes_load_scripts');


//Configurações
function comes_bebes_config()
{
    //Adicionando suporte a logo
    add_theme_support('custom-logo', array(
        'width' => 77,
        'height' => 50,
        'flex-height' => true,
        'flex-width' => true
    ));
}
add_action('after_setup_theme', 'comes_bebes_config', 0);

//Adição dos Widgets para o rodapé
function comes_bebes_widgets()
{

    $args = [
        'name' => 'Mapa e Texto',
        'id' => 'map-text',
        'description' => 'adição de texto e mapa'
    ];

    register_sidebar($args);


    $args2 = [
        'name' => 'Imagem',
        'id' => 'visit-image',
        'description' => 'Image da loja física'
    ];

    register_sidebar($args2);
}
add_action('widgets_init', 'comes_bebes_widgets');
